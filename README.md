This project is a service node in ROS. It works together with the decentealized_fleet project and is a central node to share with each agent the same knowledgement of the graph.
This node should only exists once in the setting. It will provide 3 different services, that an agent can use´.

* GetAllRoomIds - This sevrice send all the subtasks/room ids of the graph back to the agent.
* GetMapRoomCost - This will send the mapcosts of one specific room that has to be specified in the request
* GetMapPathCost - This will send the mapcosts of one specific patth that has to be specified in the request

The node gets the graph by looking in the specified ros params under "/graph_dir" to find an accessable file with a specified format.
It is Rowbased and consists of three row types.
* R|{id}|{costs} - for rooms/subtasks you can give them an id and costs which is a list sperated by comma of floats. This costs will be interpreted as an array.
* S|{id}|{costs} - for stations you can give them an id and costs which is a list sperated by comma of floats. This costs will be interpreted as an array.
* P|{id}|{pointA}|{pointB}|{cost} - for path you can give hem an id and specify which tow rooms are connteced by this path. The path in this implementation are symmetric. The costs are the same as by the room or station.

Under ./src/generate_rooms.py you find a scriptt to generate synthettic graphs by passing the desired number of rooms and stations and it will generate a graph in this text format.

