#!/usr/bin/env python
from decentralized_fleet_graph.graph import Graph, Room
from decentralized_fleet_graph.srv import GetAllRoomIds, GetAllRoomIdsResponse, GetMapCostForPath, GetMapCostForPathResponse, GetMapCostForRoom, GetMapCostForRoomResponse
import rospy







def initialize_graph():
    """
    This method looks in the ros params and extract the location of the textfile with the graph specification.
    After that it loads the graph from the textfile into the local memory.
    """
    if not rospy.has_param('/graph_dir'):
        raise Exception('I am disappointed where is my graph :O ')

    graph_dir = rospy.get_param('/graph_dir')
    graph.load_txt_file(graph_dir)


def start_Services():
    rospy.Service('getAllRoomIds', GetAllRoomIds, handle_GetAllRoomIds)
    rospy.Service('getMapCostForRoom', GetMapCostForRoom, handle_GetMapCostForRoom)
    rospy.Service('getMapCostForPath', GetMapCostForPath, handle_GetMapCostForPath)


def handle_GetAllRoomIds(request):
    room_ids = [room.element_id for room in graph.points if isinstance(room, Room)]
    return GetAllRoomIdsResponse(RoomIds=room_ids)

def handle_GetMapCostForRoom(Request):
    room = next((point for point in graph.points if point.element_id == Request.RoomId))
    return GetMapCostForRoomResponse(Cost=room.cost)

def handle_GetMapCostForPath(Request):
    try:
        path = next((path for path in graph.paths if (path.pointA == Request.FromId and path.pointB == Request.ToId)
                  or (path.pointA == Request.ToId and path.pointB == Request.FromId)))
    except:
        rospy.logerr('It was not possible to find a path from {} to {}'.format(pointA, pointB))
    return GetMapCostForPathResponse(Cost=path.cost)

graph = Graph()
if __name__ == "__main__":
    initialize_graph()
    rospy.init_node('Graph_Server')
    start_Services()
    rospy.spin()
