def map_to_cost(str_cost):
    return [float(c) for c in str_cost.split(',')]

class Graph:
    def __init__(self):
        self.points = []
        self.paths = []

    def interprete_Line(self, line):
        values = line.split('|')
        if values[0] == 'R':
            self.points.append(Room(values[1], map_to_cost(values[2])))
        if values[0] == 'S':
            self.points.append(Station(values[1], map_to_cost(values[2])))
        if values[0] == 'P':
            self.paths.append(Path(values[1], map_to_cost(values[2]), values[3], values[4]))

    def load_txt_file(self, path):
        f = open(path, 'r')
        for line in f.readlines():
            self.interprete_Line(line)


class Element:
    def __init__(self, element_id, cost):
        self.element_id = element_id
        self.cost = cost


class Room(Element):
    def __init__(self, element_id, cost):
        Element.__init__(self, element_id, cost)


class Station(Element):
    def __init__(self, element_id, cost):
        Element.__init__(self, element_id, cost)


class Path(Element):
    def __init__(self, element_id, cost, pointA, pointB):
        Element.__init__(self, element_id, cost)
        self.pointA = pointA
        self.pointB = pointB
