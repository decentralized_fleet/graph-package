import sys
import random
import numpy as np
from decentralized_fleet_graph.graph import Graph,Room, Station, Path


"""
This script can be used to generate own maps/graphs with some parameters.
The resulting graph will be printed out and can be transfered in some normal text-file.
Then this text file can be referenced by the Graph Node as src in the rosparam which leads the graph node to import this new generated graph.

Params:
./generate_rooms count_room count_stations

count_room: is to specify the number of resulting rooms/subtasks
count_stations: is to specify the number of resulting stations (start points of roboters)


"""

def generate_path(pointA, graph, connect_point , cost):
    if not any(graph.points):
        return

    for pointB in graph.points:
        if pointB.element_id == connect_point.element_id:
            add_path(graph, pointA, pointB, cost)
        else:
            connecting_path = next((path for path in graph.paths if (path.pointA == connect_point.element_id and path.pointB == pointB.element_id)
                  or (path.pointA == pointB.element_id and path.pointB == connect_point.element_id)))

            add_path(graph, pointA, pointB, connecting_path.cost + cost)


def add_path(graph, pointA, pointB, cost):
    path = Path('Path{}{}'.format(pointA.element_id, pointB.element_id), cost, pointA.element_id, pointB.element_id)
    graph.paths.append(path)




count_room = int(sys.argv[1])
count_stations = int(sys.argv[2])


graph = Graph()

for i in range(count_room):
    #cost_room = np.random.random
    cost_room = np.random.normal(loc=4, scale=2)
    room = Room('Room{}'.format(i), cost_room)

    if any(graph.points):
        generate_path(room,graph,random.choice(graph.points),np.random.random())
    graph.points.append(room)
    print('R|{}|{:.2f}|'.format(room.element_id,room.cost))


free_room_ids = [r.element_id for r in graph.points if isinstance(r, Room)]
for i in range(count_stations):
    station = Station('Station{}'.format(i), .0)

    if any(graph.points):
        connecting_room_id = random.choice(free_room_ids)
        generate_path(station, graph, next((room for room in graph.points if room.element_id == connecting_room_id)), random.random())
        free_room_ids.remove(connecting_room_id)

    graph.points.append(station)
    print('S|{}|{:.2f}|'.format(station.element_id, station.cost))

for path in graph.paths:
    print('P|{}|{:.2f}|{}|{}|'.format(path.element_id,path.cost, path.pointA, path.pointB))



